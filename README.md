Leaves grow and fall from the branch. Save them from falling into the pond by tapping on them and they blow away to safety.

Score is how many you have saved. Each leaf drops faster after each save. You can click on them when they wiggle on the branch, but it doesn't add to the score, and also reduces the duration of future wiggles for that leaf.